import { window, ExtensionContext, env, Uri } from "vscode";

// this method is called when your extension is activated
export function activate(context: ExtensionContext) {
    deprecationWarning(context);
}

async function deprecationWarning(context: ExtensionContext) {
    // show whats new notificatin:
    const actions = [{ title: "Show on Marketplace" }];

    const result = await window.showInformationMessage(
        `DEPRECATED: The Snakemake Language extension has been moved to the official Snakemake channel, please install it from there.`,
        ...actions
    );

    if (result !== null) {
        if (result === actions[0]) {
            await env.openExternal(
                Uri.parse(
                    "https://marketplace.visualstudio.com/items?itemName=Snakemake.snakemake-lang"
                )
            );
        }
    }
}
